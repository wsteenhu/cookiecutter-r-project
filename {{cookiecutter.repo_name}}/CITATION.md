Please cite this project as follows:

{{ cookiecutter.full_name }} ({% now 'local', '%Y' %}),  {{cookiecutter.project_name}} - version {{cookiecutter.version}}. url: gitlab.com/{{cookiecutter.gitlab_username}}/{{cookiecutter.repo_name}}
