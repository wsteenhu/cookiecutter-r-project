# Cookiecutter R-project (template)

A cookiecutter project structure based on the [Reproducible Science Cookiecutter](https://github.com/mkrapp/cookiecutter-reproducible-science) template by [Mario Krapp](https://github.com/mkrapp), which was adusted by [Barbara Vreede](https://github.com/bvreede) and was originally named [`good-enough-project`](https://github.com/bvreede/good-enough-project). This structure adheres to the philosophy of [Cookiecutter Data Science](https://github.com/drivendata/cookiecutter-data-science): *A logical, reasonably standardized, but flexible project structure for doing and sharing data science work.*

The original name and structure are based on the paper [Good Enough Practices in Scientific Computing](https://doi.org/10.1371/journal.pcbi.1005510), Wilson _et al._, PLOS Computational Biology (2017).

## Requirements

Install `cookiecutter` on the command line with pip `pip install cookiecutter` or with conda `conda install cookiecutter`.
Some instructions on pip/conda can be found [here](https://utrechtuniversity.github.io/workshop-computational-reproducibility/preparations).

## Usage

To start a new science project:

`cookiecutter gl:wsteenhu/cookiecutter-r-project`

Answer the questions cookiecutter asks you. If you're happy with a default answers, fill in nothing and hit `<return>`.

Compared to the previous [version](https://github.com/bvreede/good-enough-project), the current cookiecutter template is more catered towards R-projects, dropping unused directories (e.g. `bin/`), adding the `scripts/` folder for `.Rmd`-files and adding a `.Rproj`-file by default. The current cookiecutter template also adds a first `.Rmd` file to the `scripts/`-folder, with paths for output already set correctly. This file serves as a template and can be copied for subsequent subanalyses.

After initiating a project in this way, consider:
- running `git init` in the terminal inside the project root-directory.
- running `renv::init()` after starting up the project in RStudio for local R dependency management of projects (see the documentation on [`renv`](https://rstudio.github.io/renv/articles/renv.html)).

For configuration of default values (with your name/e-mail/GitLab prespecified) consider adding a [`.cookiecutterrc`](https://cookiecutter.readthedocs.io/en/1.7.0/advanced/user_config.html)-file to your home directory.

## Project Structure

The project structure distinguishes three kinds of folders:
- read-only (RO): not edited by either code or researcher
- human-writeable (HW): edited by the researcher only.
- project-generated (PG): folders generated when running the code; these folders can be deleted or emptied and will be completely reconstituted as the project is run.

```
.
├── .gitignore
├── .Rprofile
├── CITATION.md
├── LICENSE.md
├── README.md
├── <repo_name>.Rproj
├── config                <- Configuration files (HW)
├── data                  <- All project data, ignored by git
│   ├── processed         <- The final, canonical data sets for modeling (PG)
│   └── raw               <- The original, immutable data dump (e.g. OTU-tables/unprocessed metadata) (RO)
├── doc                   <- Documentation/supporting information (HW)
│   └── manuscript        <- Manuscript source (HW)
├── ext                   <- External data files; e.g. databases (HW)
├── results               <- Output generated from .Rmd-files (PG)
│   ├── figures           <- Figures for the manuscript (PG)
│   ├── reports           <- Reports generated by .Rmd (typically .html/.pdf) (PG)
│   └── tables            <- Tables for the manuscript (PG)
├── scripts               <- .Rmd-files; these may source from src/ (HW)
│   └── <subdir_name>.Rmd <- Exemplary .Rmd-file with paths set correctly (HW)
└── src                   <- Source code for this project; .R-files (HW)

```

## License

This project is licensed under the terms of the [MIT License](/LICENSE.md).
